#!/usr/bin/python3
from flask import Flask, Response, flash, redirect, request, url_for
from flask_cors import CORS
import os
import json
from werkzeug.utils import secure_filename

# custom modules
import objdet

"""
to start run:
export FLASK_APP=flaskController.py
export FLASK_DEBUG=1 (optional, to reload upon changes automatically)
python -m flask run

example calls:

curl -X GET localhost:5000/detectLanguages?input="aap"
curl -F 'pdffile=@temp_pdf_storage/machine_readable_single_column_2.pdf' -X POST localhost:5000/ocr
"""

app = Flask(__name__)
app.secret_key = "super secret key"
CORS(app)

@app.route('/welcome', methods=['GET'])
def dummy():
    return "Hello stranger, can you tell us where you've been?\nMore importantly, how ever did you come to be here?\n"

##################### object detection #####################
@app.route('/detectObjects', methods=['POST'])
def detectObjects():

    f = request.files['file']
    f.save(secure_filename(f.filename+".jpg"))

    print(f.filename+".jpg")
    
    dict = objdet.detect_objects(f.filename+".jpg")
    print(dict);
    return Response(response=json.dumps(dict), status=200, content_type='application/json')
    #return Response(response='salida', status=200, content_type='text/plain')

    """
    if request.method == 'POST':
        ctype = request.headers["Content-Type"]
        print("Content type: {}".format(ctype))
        accept = request.headers["Accept"]
        print("Response type: {}".format(accept))

        if not request.data:
            print("Error: No request body provided in message")

        request_body = request.data.decode('utf-8')
        detected_objects = objectDetectionRun(request_body)
        response_dict = {
                "objects": [
                ]
        }
        return Response(response=json.dumps(response_dict), status=200, content_type='application/json')
    else:
        return Response(response="{'error':'Only POST method allowed.'}", status=400, content_type='application/json')
    """


if __name__ == '__main__':
    #port = int(os.environ.get('PORT', 8080))
    #app.run(host='localhost', port=port, debug=True)
    dict = objdet.detect_objects('dog1.jpg')

