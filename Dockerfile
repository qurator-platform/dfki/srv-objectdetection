FROM python:3.6-stretch
LABEL maintainer="julian.moreno_schneider@dfki.de"

RUN apt-get -y update && \
    apt-get upgrade -y && \
    apt-get install -y python3-dev &&\
    apt-get update -y

RUN mkdir objdet/ && chmod 777 objdet
WORKDIR objdet

ADD requirements.txt .
RUN pip3 install -r requirements.txt
#RUN python3 -m nltk.downloader punkt

#ADD bert-base-cased /custombert/bert-base-cased
#ADD nermodels/wikiner-en nermodels/wikiner-en

ADD objdet.py .
ADD model.h5 .
ADD yolov3.weights .
ADD convert.py .
ADD flaskController.py .

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

EXPOSE 8080

ENTRYPOINT FLASK_APP=flaskController.py flask run --host=0.0.0.0 --port=8080
#CMD ["/bin/bash"]
